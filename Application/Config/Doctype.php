<?php 
/************************************************************/
/*                  DOCTYPE(DÖKÜMAN TÜRÜ)                   */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

/******************************************************************************************
* DOCTYPE                                                                         	  	  *
*******************************************************************************************
| Genel Kullanım: Döküman türleri listesi.      			  							  |
******************************************************************************************/
$config['Doctype']['xhtml1Strict'] 			= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//TR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
$config['Doctype']['xhtml1Transitional'] 	= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//TR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
$config['Doctype']['xhtml1Frameset'] 		= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//TR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
$config['Doctype']['xhtml11'] 				= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//TR" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
$config['Doctype']['html4Strict'] 			= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//TR" "http://www.w3.org/TR/html4/strict.dtd">';
$config['Doctype']['html4Transitional'] 	= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//TR" "http://www.w3.org/TR/html4/loose.dtd">';
$config['Doctype']['html4Frameset'] 		= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//TR" "http://www.w3.org/TR/html4/frameset.dtd">';
$config['Doctype']['html5'] 				= '<!DOCTYPE html>';