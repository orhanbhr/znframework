<?php
/************************************************************/
/*                         LINKS                            */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

/******************************************************************************************
* SCRIPT                                                                         	  	  *
*******************************************************************************************
| Genel Kullanım: Script URL bilgilerini tutmak için oluşturulmuştur.           	      |
| Bu linkleri güncelleyerek jquery'nin en son sürümlerini kullanabilirsiniz.			  |
| Bu scriptleri import ederken anahtar ifadeler kullanılarak dahil etme işlemi yapılır.   |
| Örnek Kullanım: Import::script('jqueryUi');											  |
******************************************************************************************/
$config['Links']['script'] = array
(
	'jquery'   => 'https://code.jquery.com/jquery-latest.js',
	'jqueryUi' => 'https://code.jquery.com/ui/1.11.3/jquery-ui.js'
);

/******************************************************************************************
* STYLE                                                                         	  	  *
*******************************************************************************************
| Genel Kullanım: Style URL bilgilerini tutmak için oluşturulmuştur.               	      |
| Bu linkleri güncelleyerek dışardan style dosyaları çağırabilirsiniz.					  |
| Bu stilleri import ederken anahtar ifadeler kullanılarak dahil etme işlemi yapılır.     |
| Örnek Kullanım: Import::style('style');									     		  |
******************************************************************************************/
$config['Links']['style'] = array();