<?php 
/************************************************************/
/*                         COMPOSER                         */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

/******************************************************************************************
* COMPOSER AUTOLOAD                                                                       *
*******************************************************************************************
| Genel Kullanım: Composer autoload dosyasının dahil edilip edilmeyeceğine karar verir.   |
| Parametre: True, false veya yol değeri alır. True, vendor/autoload.php dosyasının       |
| dahil edilmesi anlamına gelir. Parametre olarak yol değeri belirtilebilir. 			  |
| Örnek: example/vendor/autoload.php													  |
| Örnek: true veya false														          |
******************************************************************************************/	
$config['Composer']['autoload'] = false;
//--------------------------------------------------------------------------------------------------------------------------