<?php
/************************************************************/
/*                   	  ZN LIBRARY                        */
/************************************************************/
/*
/* Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
/* Site: www.zntr.net
/* Lisans: The MIT License
/* Telif Hakkı: Copyright (c) 2012-2015, zntr.net
*/
/******************************************************************************************
* CONTROLLER CLASS                                                                        *
*******************************************************************************************
| Küresel değişken kullanmak için oluşturulmuştur.										  |
******************************************************************************************/	
class ZN
{
	/* Use Global Değişkeni
	 *  
	 * $this nesnesine ait bilgileri
	 * barındırmak için oluşturulmuştur.
	 *
	 */
	public static $use;	
}