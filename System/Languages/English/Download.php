<?php
/************************************************************/
/*                   DOWNLOAD LANGUAGE                      */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Download']['emptyParameterError'] = 'Could not find the file download process will be carried out!';
$lang['Download']['stringParameterError'] = 'File path parameter should contain string data type!';