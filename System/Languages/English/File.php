<?php
/************************************************************/
/*                     FILE LANGUAGE                        */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['File']['notFoundError'] = '`%` file could not be found!';
$lang['File']['alreadyFileError'] = '`%` file already exists!';
$lang['File']['remoteUploadError'] = '`%` file is not installed on the server!';
$lang['File']['remoteDownloadError'] = '`%` file is not downloaded from the server!';
