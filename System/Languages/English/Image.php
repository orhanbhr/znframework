<?php
/************************************************************/
/*                    IMAGE LANGUAGE                        */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Image']['notFoundError'] = '`%` file could not be found!';
$lang['Image']['notImageFileError'] = '`%` file is not an image file!';
