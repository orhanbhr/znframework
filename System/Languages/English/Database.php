<?php
/************************************************************/
/*                   DATABASE LANGUAGE                      */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
// ERROR ------------------------------------------------------------------------------------------------------------------
$lang['Database']['parameterError'] = '% parameter missing or empty!';
$lang['Database']['tableNotExistsError'] = '';
$lang['Database']['tableAlreadyExistsError'] = '';
$lang['Database']['dbAlreadyExistsError'] = '';
$lang['Database']['setValueError'] = '';
$lang['Database']['insertValueError'] = '';
$lang['Database']['unknownColumnError'] = '';
$lang['Database']['SQLSytaxError'] = '';
$lang['Database']['classNotExistsError'] = '% parameter class information not found!';
$lang['Database']['mysqlConnectError'] = 'HATA: Database connection error! Please check your connection settings.';
$lang['Database']['stringError'] = '% parameter query should include the source variable!';
$lang['Database']['connectResourceError'] = '% parameter connection resource variable must contain!';
$lang['Database']['stringParameterError'] = '% parameter should contain string data type!';
$lang['Database']['numericParameterError'] = '% parameter should contain numeric data type!';
$lang['Database']['arrayParameterError'] = '% parameter should contain array data type!';
$lang['Database']['arrayStringParameterError'] = '%  parameter should contain array or string data type!';
$lang['Database']['driverError'] = '`%` driver not found!';
// ERROR ------------------------------------------------------------------------------------------------------------------

// SUCCESS ----------------------------------------------------------------------------------------------------------------
$lang['Database']['optimizeTablesSuccess'] = 'The optimization process was completed successfully.';
$lang['Database']['backupTablesSuccess'] = 'The backup process was completed successfully.';
$lang['Database']['repairTablesSuccess'] = 'The repair process was completed successfully.';
// SUCCESS ----------------------------------------------------------------------------------------------------------------