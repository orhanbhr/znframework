<?php
/************************************************************/
/*                   BENCHMARK LANGUAGE                     */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Benchmark']['elapsedTime'] 		= 'System Load Time';
$lang['Benchmark']['memoryUsage'] 		= 'Memory Usage';
$lang['Benchmark']['maxMemoryUsage'] 	= 'Maximum Memory Usage';
$lang['Benchmark']['resultTable']		= 'BENCHMARK RESULT TABLE';
$lang['Benchmark']['performanceTips']	= 'PERFORMANCE ENHANCING TIPS';
$lang['Benchmark']['laterProcess']		= 'Use the following settings are recommended after completion of your project.';
$lang['Benchmark']['configAutoloader']	= 'Config/Autoloader.php';
$lang['Benchmark']['configHtaccess']	= 'Config/Htaccess.php';
$lang['Benchmark']['second']			= 'Seconds';
$lang['Benchmark']['byte']				= 'Bytes';