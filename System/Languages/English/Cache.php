<?php
/************************************************************/
/*                     CACHE LANGUAGE                       */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
// ERROR ------------------------------------------------------------------------------------------------------------------
$lang['Cache']['driverError'] = '`%` driver not found!';
$lang['Cache']['unsupported'] = '`%` PHP extension must be loaded to use Wincache Cache!';
$lang['Cache']['invalidDriver'] = '`%` driver is invalid!';
$lang['Cache']['connectionRefused'] = 'Connection refused! Error:`%`';
$lang['Cache']['authenticationFailed'] = 'Authentication failed!';