<?php
/************************************************************/
/*                     COOKIE LANGUAGE                      */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Cookie']['nameParameterEmptyError'] = 'Cookie name parameter is empty!';
$lang['Cookie']['valueParameterEmptyError'] = 'Cookie value parameter is empty!';
$lang['Cookie']['setError'] = 'Could not set the cookie!';
$lang['Cookie']['notSelectError'] = 'Cookie could not be selected or does not exist!';
$lang['Cookie']['notDeleteError'] = 'Cookie could not be deleted or does not exist!';