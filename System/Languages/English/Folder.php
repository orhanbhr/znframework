<?php
/************************************************************/
/*                    FOLDER LANGUAGE                       */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Folder']['notFoundError'] = '`%` directory could not be found!';
$lang['Folder']['alreadyFileError'] = '`%` directory already exists!';
$lang['Folder']['parameterError'] = '`%` path to the directory must contain the information!';
$lang['Folder']['changeFolderError'] = '`%` Can not change the working directory!';
$lang['Folder']['changeFolderNameError'] = ' The name of the `%` file can not be changed!';
