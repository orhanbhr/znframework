<?php
/************************************************************/
/*                   DATABASE LANGUAGE                      */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
// ERROR ------------------------------------------------------------------------------------------------------------------
$lang['Database']['parameterError'] = '% parametre eksik veya boş!';
$lang['Database']['tableNotExistsError'] = '`%` tablosu bulunamadı!';
$lang['Database']['tableAlreadyExistsError'] = '`%` tablosu zaten var!';
$lang['Database']['dbAlreadyExistsError'] = '`%` veritabanı zaten var!';
$lang['Database']['setValueError'] = 'Güncellenecek bilgiler parametresi boş!';
$lang['Database']['insertValueError'] = 'Eklenecek bilgiler parametresi boş!';
$lang['Database']['unknownColumnError'] = 'Bilinmeyen sütun!';
$lang['Database']['unknown_database_error'] = '`%` bilinmeyen bir veritabanıdır!';
$lang['Database']['not_database_selected_error'] = 'Herhangi bir veritabanı seçilmedi!';
$lang['Database']['SQLSytaxError'] = 'SQL yazım hatası';
$lang['Database']['query_empty_error'] = 'Sorgu boş!';
$lang['Database']['stringError'] = '% parametre sorgu kaynak değişkeni içermelidir!';
$lang['Database']['connectResourceError'] = '% parametre bağlantı kaynak değişkeni içermelidir!';
$lang['Database']['classNotExistsError'] = '% parametre sınıf bilgisi bulunamadı!';
$lang['Database']['invalid_charset_error'] = 'Böyle bir karakter seti bulunamadı!';
$lang['Database']['mysqlConnectError'] = 'HATA: Veritabanı bağlantısı sağlanamadı! Lütfen bağlantı ayarlarınızı kontrol edin.';
$lang['Database']['stringParameterError'] = '% parametre metinsel veri türü içermelidir!';
$lang['Database']['numericParameterError'] = '% parametre numerik veri türü içermelidir!';
$lang['Database']['arrayParameterError'] = '% parametre dizi veri türü içermelidir!';
$lang['Database']['arrayStringParameterError'] = '% parametre dizi veya metinsel veri türü içermelidir!';
$lang['Database']['driverError'] = '`%` sürücüsü bulunamadı!';
$lang['Database']['dublicate_column_error'] = '`%` sütunu zaten var!';
$lang['Database']['no_drop_column_error'] = '`%` sütunu daha önce kaldırıldığı için silinemiyor!';
// ERROR ------------------------------------------------------------------------------------------------------------------

// SUCCESS ----------------------------------------------------------------------------------------------------------------
$lang['Database']['optimizeTablesSuccess'] = 'Optimizasyon işlemi başarı ile tamamlandı.';
$lang['Database']['backupTablesSuccess'] = 'Yedekleme işlemi başarı ile tamamlandı.';
$lang['Database']['repairTablesSuccess'] = 'Onarma işlemi başarı ile tamamlandı.';
// SUCCESS ----------------------------------------------------------------------------------------------------------------