<?php
/************************************************************/
/*                   DOWNLOAD LANGUAGE                      */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Download']['emptyParameterError'] = 'İndirme işlemi gerçekleştirilecek dosya bulunamadı!';
$lang['Download']['stringParameterError'] = 'Dosya yolu parametresi metinsel veri türü içermelidir!';
