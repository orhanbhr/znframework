<?php
/************************************************************/
/*                     CACHE LANGUAGE                       */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
// ERROR ------------------------------------------------------------------------------------------------------------------
$lang['Cache']['driverError'] = '`%` sürücüsü bulunamadı!';
$lang['Cache']['unsupported'] = '`%` sürücünü kullanmak için yüklenmesi gerekmektedir!';
$lang['Cache']['invalidDriver'] = '`%` sürücüsü geçersizdir!';
$lang['Cache']['connectionRefused'] = 'Bağlantı sağlanamadı! Hata:`%`';
$lang['Cache']['authenticationFailed'] = 'Geçersiz giriş!';