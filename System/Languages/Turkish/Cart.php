<?php
/************************************************************/
/*                     CART LANGUAGE                        */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Cart']['arrayParameterError'] = 'Eklenecek veri parametresi dizi bilgisi içermelidir!';
$lang['Cart']['updateArrayParameterError'] = 'Güncellenecek veri parametresi dizi bilgisi içermelidir!';
$lang['Cart']['insertParameterEmptyError'] = 'Eklenecek veri parametresi boş!';
$lang['Cart']['updateParameterEmptyError'] = 'Güncellenecek veri parametresi boş!';
$lang['Cart']['noDataError'] = 'Eklenmiş olan herhangi bir veri yok!';
$lang['Cart']['updateCodeError'] = 'Güncelleme işlemini sağlayacak anahtar ürün bilgisi boş!';
$lang['Cart']['deleteCodeError'] = 'Silme işlemini sağlayacak anahtar ürün bilgisi boş!';