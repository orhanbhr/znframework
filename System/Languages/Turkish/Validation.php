<?php
/************************************************************/
/*                   VALIDATION LANGUAGE                    */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Validation']['required'] = '% alanı boş geçilemez!';
$lang['Validation']['passwordMatch'] = 'Şifreler uyumsuz!';
$lang['Validation']['dataMatch'] = '% bilgileri uyumsuz!';
$lang['Validation']['oldPasswordMatch'] = 'Eski şifre yanlış!';
$lang['Validation']['email'] = '% alanı geçersiz posta adresidir!';
$lang['Validation']['url'] = '% alanı geçersiz url bilgisidir!';
$lang['Validation']['identity'] = '% alanı geçersiz kimlik bilgisidir!';
$lang['Validation']['noSpecialChar'] = '% alanı özel karakter içeremez!';
$lang['Validation']['numeric'] = '% alanı sadece sayılardan oluşmalıdır!';
$lang['Validation']['maxchar'] = '% alanı en fazla # karakterden oluşmalıdır!';
$lang['Validation']['minchar'] = '% alanı en az # karakterden oluşmalıdır!';
$lang['Validation']['captchaCode'] = 'Güvenlik kodu hatalı!';