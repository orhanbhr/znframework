<?php
/************************************************************/
/*                     COOKIE LANGUAGE                      */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Cookie']['nameParameterEmptyError'] = 'Çerez isim parametresi boş!';
$lang['Cookie']['valueParameterEmptyError'] = 'Çerez değer parametresi boş!';
$lang['Cookie']['setError'] = 'Çerez tanımlanamadı!';
$lang['Cookie']['notSelectError'] = 'Çerez seçilemedi veya böyle bir çerez yok!';
$lang['Cookie']['notDeleteError'] = 'Çerez silinemedi veya böyle bir çerez yok!';