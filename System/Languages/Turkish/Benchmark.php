<?php
/************************************************************/
/*                   BENCHMARK LANGUAGE                     */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Benchmark']['elapsedTime'] 		= 'Sistem Yüklenme Süresi';
$lang['Benchmark']['memoryUsage'] 		= 'Hafıza Kullanımı';
$lang['Benchmark']['maxMemoryUsage'] 	= 'Azami Hafıza Kullanımı';
$lang['Benchmark']['resultTable']		= 'BENCHMARK SONUÇ TABLOSU';
$lang['Benchmark']['performanceTips']	= 'PERFORMANS ARTIRMA İPUÇLARI';
$lang['Benchmark']['laterProcess']		= 'Projenizin tamamlanmasından sonra aşağıdaki ayarların kullanımı önerilir.';
$lang['Benchmark']['configAutoloader']	= 'Config/Autoloader.php';
$lang['Benchmark']['configHtaccess']	= 'Config/Htaccess.php';
$lang['Benchmark']['second']			= 'Saniye';
$lang['Benchmark']['byte']				= 'Bayt';