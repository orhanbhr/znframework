<?php
/************************************************************/
/*                    FOLDER LANGUAGE                       */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['Folder']['notFoundError'] = '`%` dizini bulunamadı!';
$lang['Folder']['alreadyFileError'] = '`%` dizini zaten var!';
$lang['Folder']['parameterError'] = '`%` yolu dizin bilgisi içermelidir!';
$lang['Folder']['changeFolderError'] = '`%` çalışma dizini olarak değiştirilemiyor!';
$lang['Folder']['changeFolderNameError'] = '`%` dosyasının adı değiştirilemiyor!';
