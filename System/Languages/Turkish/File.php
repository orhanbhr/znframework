<?php
/************************************************************/
/*                     FILE LANGUAGE                        */
/************************************************************/
/*

Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
Site: http://www.zntr.net
Copyright 2012-2015 zntr.net - Tüm hakları saklıdır.

*/
$lang['File']['notFoundError'] = '`%` dosyası bulunamadı!';
$lang['File']['alreadyFileError'] = '`%` dosyası zaten var!';
$lang['File']['remoteUploadError'] = '`%` dosyası sunucuya yüklenemiyor!';
$lang['File']['remoteDownloadError'] = '`%` dosyası sunucudan indirilemiyor!';
