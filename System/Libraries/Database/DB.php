<?php
class __USE_STATIC_ACCESS__DB
{	
	/***********************************************************************************/
	/* DB LIBRARY						                   	                           */
	/***********************************************************************************/
	/* Yazar: Ozan UYKUN <ozanbote@windowslive.com> | <ozanbote@gmail.com>
	/* Site: www.zntr.net
	/* Lisans: The MIT License
	/* Telif Hakkı: Copyright (c) 2012-2015, zntr.net
	/*
	/* Sınıf Adı: DB
	/* Versiyon: 1.2
	/* Tanımlanma: Dinamik
	/* Dahil Edilme: Gerektirmez
	/* Erişim: $this->db, zn::$use->db, uselib('db')
	/* Not: Büyük-küçük harf duyarlılığı yoktur.
	/***********************************************************************************/
	
	use DBCommonTrait;
	use DBControlFlowTrait;
	use DBVariableTypesTrait;
	use DBMathFunctionsTrait;
	use DBOtherFunctionsTrait;
	use DBConversionExpressionEvaluationTrait;
	use DBStringFunctionsTrait;
	use DBSelectClausesTrait;
	use DBMiscellaneousFunctionsTrait;
	use DBDateTimeFunctionsTrait;
	use DBFullTextFunctionsTrait;
	use DBXMLFunctionsTrait;
	use DBEncryptionCompressionFunctionsTrait;
	use DBInformationFunctiosTrait;
	use DBSpatialAnalysisFunctionsTrait;
	use DBJsonFunctionsTrait;
	use DBGlobalTransactionFunctionsTrait;
	use DBEnterpriseEncryptionFunctionsTrait;
	use DBAggregateFunctionsTrait;
	
	/* Select Değişkeni
	 *  
	 * SELECT bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $select;
	
	/* Select Column Değişkeni
	 *  
	 * col1, col2 ... bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $selectColumn;
	
	/* From Değişkeni
	 *  
	 * FROM bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $from;
	
	/* Where Değişkeni
	 *  
	 * WHERE bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $where;
	
	/* All Değişkeni
	 *  
	 * ALL bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $all;
	
	/* Distinct Değişkeni
	 *  
	 * DISTINCT bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $distinct;
	
	/* Distinct Row Değişkeni
	 *  
	 * DISTINCTROW bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $distinctRow;
	
	/* High Priority Değişkeni
	 *  
	 * HIGH PRIORITY bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $highPriority;
	
	/* Straight Join Değişkeni
	 *  
	 * STRAIGHT JOIN bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $straightJoin;
	
	/* Small Result Değişkeni
	 *  
	 * SMALL RESULT bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $smallResult;	
	
	/* Big Result Değişkeni
	 *  
	 * BIG RESULT bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */	
	private $bigResult;
	
	/* Buffer Result Değişkeni
	 *  
	 * BUFFER RESULT bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */			
	private $bufferResult;	
	
	/* Cache Değişkeni
	 *  
	 * CACHE bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */	
	private $cache;	
	
	/* No Cache Değişkeni
	 *  
	 * NO CACHE bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */	
	private $noCache;
	
	/* Calc Found Rows Değişkeni
	 *  
	 * CALC FOUND ROWS bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */	
	private $calcFoundRows;	
	
	/* Math Değişkeni
	 *  
	 * Matemetiksel işlem bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $math;
	
	/* Group By Değişkeni
	 *  
	 * GROUP BY bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $groupBy;
	
	/* Having Değişkeni
	 *  
	 * HAVING bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $having;
	
	/* Order By Değişkeni
	 *  
	 * ORDER BY bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $orderBy;
	
	/* Limit Değişkeni
	 *  
	 * LIMIT bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $limit;
	
	/* Join Değişkeni
	 *  
	 * JOIN bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $join;
	
	/* Trans Start Değişkeni
	 *  
	 * Çoklu sorgu işlem bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $transStart;
	
	/* Trans Error Değişkeni
	 *  
	 * Çoklu sorgu işlem hata bilgisini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $transError;	
	
	/* Pagination Değişkeni
	 *  
	 * Sayfalama ayar bilgilerini
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $pagination = array('start' => 0, 'limit' => 0);
	
	/* Unlimited Total Rows Değişkeni
	 *  
	 * Limit değerine rağmen toplam kayıt sayısını
	 * tutmak için oluşturulmuştur.
	 *
	 */
	private $unlimitedTotalRows;
	
	/******************************************************************************************
	* CALL                                                                                    *
	*******************************************************************************************
	| Genel Kullanım: Geçersiz fonksiyon girildiğinde çağrılması için.						  |
	|          																				  |
	******************************************************************************************/
	public function __call($method = '', $param = '')
	{	
		die(getErrorMessage('Error', 'undefinedFunction', "DB::$method()"));	
	}
	
	/******************************************************************************************
	* SELECT                                                                                  *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde SELECT kullanımı için oluşturulmuştur.				  |
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. string var @condition => Sütun bilgileri parametresidir. Varsayılan:*		    	  |
	|          																				  |
	| Örnek Kullanım: ->select('col1, col2 ...')        									  |
	|          																				  |
	******************************************************************************************/
	public function select($condition = '*')
	{
		if( ! is_string($condition) ) 
		{
			$condition = '*';
		}
		
		$args = func_get_args();
		
		if( count($args) > 1 )
		{
			$condition = rtrim(implode(',', $args), ',');
		}
		
		$this->selectColumn = ' '.$condition.' ';
		$this->select = 'SELECT';
		
		return $this;
	}
	
	/******************************************************************************************
	* FROM                                                                                    *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde FROM kullanımı için oluşturulmuştur.				  |
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. string var @table => Tablo adı parametresidir.                                       |
	|          																				  |
	| Örnek Kullanım: ->from('OrnekTablo')		        									  |
	|          																				  |
	******************************************************************************************/
	public function from($table = '')
	{
		if( is_string($table) ) 
		{
			$this->from = ' FROM '.$this->prefix.$table.' ';
			$this->tableName = $this->prefix.$table;
		}
		else
		{
			Error::set(lang('Error', 'stringParameter', 'table'));	
		}
		
		return $this;
	}
	
	/******************************************************************************************
	* WHERE                                                                                   *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde WHERE kullanımı için oluşturulmuştur.				  |
	|															                              |
	| Parametreler: 3 parametresi vardır.                                                     |
	| 1. string var @column => Sütun ve operatör parametresidir.                              |
	| 2. string var @value => Karşılaştırılacak sütun değeri.                                 |
	| 3. [ string var @logical ] => Bağlaç bilgisi. AND, OR                                   |
	|          																				  |
	| 3. Parametre çoklu koşul gerektiğinde kullanılır.             						  |
	|          																				  |
	| Örnek Kullanım: ->where('id >', 2, 'and')->where('id <', 20);		        			  |
	| Örnek Kullanım: ->where('isim =', 'zntr', 'or')->where('isim = ', 'zn')		          |
	|          																				  |
	******************************************************************************************/
	public function where($column = '', $value = '', $logical = '')
	{
		// Parametrelerin string kontrolü yapılıyor.
		if( ! is_string($column) || ! isValue($value) || ! is_string($logical) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'column, value, logical'));
		}
		else
		{
			$value = presuffix($this->db->realEscapeString($value), "'");
			
			$this->where .= ' '.$column.' '.$value.' '.$logical.' ';
		}
		
		return $this;
	}
	
	
	/******************************************************************************************
	* HAVING                                                                                  *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde HAVING kullanımı için oluşturulmuştur.				  |
	|															                              |
	| Parametreler: 3 parametresi vardır.                                                     |
	| 1. string var @column => Sütun ve operatör parametresidir.                              |
	| 2. string var @value => Karşılaştırılacak sütun değeri.                                 |
	| 3. [ string var @logical ] => Bağlaç bilgisi. AND, OR                                   |
	|          																				  |
	| 3. Parametre çoklu kullanım gerektiğinde kullanılır.             						  |
	|          																				  |
	| Örnek Kullanım: ->having('count(*) >', 1)                   		        		      |
	|          																				  |
	******************************************************************************************/
	public function having($column = '', $value = '', $logical = '')
	{
		// Parametrelerin string kontrolü yapılıyor.
		if( ! is_string($column) || ! isValue($value) || ! is_string($logical) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'column, value, logical'));
		}
		else
		{
			$value = presuffix($this->db->realEscapeString($value), "'");

			$this->having .= ' '.$column.' '.$value.' '.$logical.' ';
		}
		
		return $this;
	}
	
	/******************************************************************************************
	* JOIN                                                                                    *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde JOIN kullanımı için oluşturulmuştur.				  |
	|															                              |
	| Parametreler: 3 parametresi vardır.                                                     |
	| 1. string var @table => Birleştirme yapılacak tablo ismi.                               |
	| 2. string var @condition => Karşılaştırılacak sütun değerleri.                          |
	| 3. string var @logical => Birleştirme türü. LEFT, RIGHT, INNER                          |
	|          																				  |
	| Örnek Kullanım: ->join('OrnekTablo', 'DenemeTablo.id = OrnekTablo.id', 'inner')         |
	|          																				  |
	******************************************************************************************/
	public function join($table = '', $condition = '', $type = '')
	{
		// Parametrelerin string kontrolü yapılıyor.
		if( ! is_string($table) || ! is_string($condition) || ! is_string($type) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'table, condition, type'));
		}
		else
		{
			$this->join .= ' '.$type.' JOIN '.$table.' ON '.$condition.' ';
		}
		
		return $this;
	}
	
	/******************************************************************************************
	* GET                                                                                    *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerini tamamlamak için oluşturulmuştur.				      |
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. [ string var @table ] => Tablo ismi.form() yöntemine alternatif olarak kullanılabilir|
	|          																				  |
	| Örnek Kullanım: ->get('OrnekTablo');        											  |
	|          																				  |
	******************************************************************************************/
	public function get($table = '')
	{
		if( ! is_string($table) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'table'));
			
			return $this;
		}
		
		if( empty($this->select) )
		{ 
			$this->select = 'SELECT'; 
			
			if( empty($this->math) )
			{
				$this->selectColumn = ' * ';
			}
			else
			{
				$this->selectColumn = ' ';	
			}
		}
				
		if( ! empty($table) ) 
		{
			$this->from = ' FROM '.$this->prefix.$table.' ';
			$this->tableName = $this->prefix.$table;
		}
		elseif( ! empty($this->table) )
		{
			$this->from = ' FROM '.$this->prefix.$this->table.' ';
		}
		
		// Çoklu WHERE kullanımından dolayı
		// Son ek kontrolü yapılıyor.
		if( ! empty($this->where) )
		{
			 $where = ' WHERE '; 
			
			if( strtolower(substr(trim($this->where), -2)) === 'or' )
			{
				$this->where = substr(trim($this->where), 0, -2);
			}
			
			if( strtolower(substr(trim($this->where), -3)) === 'and' )
			{
				$this->where = substr(trim($this->where), 0, -3);		
			}
		}
		else 
		{
			$where = '';
		}
		
		// Çoklu HAVING kullanımından dolayı
		// Son ek kontrolü yapılıyor.
		if( ! empty($this->having) ) 
		{
			$having = ' HAVING '; 
			
			if( strtolower(substr(trim($this->having), -2)) === 'or' )
			{
				$this->having = substr(trim($this->having), 0, -2);
			}
			
			if( strtolower(substr(trim($this->having), -3)) === 'and' )
			{
				$this->having = substr(trim($this->having), 0, -3);	
			}
		}
		else 
		{
			$having = '';
		}
		
		// Sorgu yöntemlerinden gelen değeler birleştiriliyor.
		
		$paginationQueryBuilder = $this->select.
								  $this->all.
								  $this->distinct.
								  $this->distinctRow.
							 	  $this->highPriority.
								  $this->straightJoin.
								  $this->smallResult.
								  $this->bigResult.
								  $this->bufferResult.
								  $this->cache.
								  $this->noCache.
								  $this->calcFoundRows.					 
								  $this->selectColumn.
								  $this->math.
								  $this->from.
								  $this->join.
								  $where.$this->where.
								  $this->groupBy.
								  $having.$this->having.
								  $this->orderBy;
		
		$queryBuilder = $paginationQueryBuilder.$this->limit;
		
		$this->_resetQuery();

		$secure = $this->secure;
		$this->unlimitedTotalRows = $this->query($this->_querySecurity($paginationQueryBuilder), $secure)->totalRows();
		$this->db->query($this->_querySecurity($queryBuilder), $secure);

		$this->unlimitedStringQuery = $paginationQueryBuilder;
		
		return $this;
	}
	
	/******************************************************************************************
	* QUERY                                                                                   *
	*******************************************************************************************
	| Genel Kullanım: Standart veritabanı sorgusu kullanmak için oluşturulmuştur.			  |
	|															                              |
	| Parametreler: 2 parametresi vardır.                                                     |
	| 1. string var @query  => SQL SORGULARI yazılır.							              |
	| 2. string var @secure  => Sorgu güvenliği içindir.						              |
	|          																				  |
	| Örnek Kullanım: $this->db->query('SELECT * FROM OrnekTablo');        					  |
	|          																				  |
	******************************************************************************************/
	public function query($query = '', $secure = array())
	{
		if( ! is_string($query) || empty($query) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'query'));
			Error::set(lang('Error', 'emptyParameter', 'query'));
		}
		else
		{
			if( isset($this->secure) )
			{
				$secure = $this->secure;
			}
			
			$this->db->query($this->_querySecurity($query), $secure);
			
			if( ! empty($this->transStart) ) 
			{
				$transError = $this->db->error();
				if( ! empty($transError) ) 
				{
					$this->transError = $transError; 
				}
			}
		}
		
		return $this;
	}
	
	/******************************************************************************************
	* EXEC QUERY                                                                              *
	*******************************************************************************************
	| Genel Kullanım: Standart veritabanı sorgusu kullanmak için oluşturulmuştur.			  |
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. string var @query  => SQL SORGULARI yazılır.							              |
	| 2. string var @secure  => Sorgu güvenliği içindir.						              |
	|          																				  |
	| Örnek Kullanım: $this->db->execQuery('DROP TABLE OrnekTablo');        			      |
	|          																				  |
	******************************************************************************************/
	public function execQuery($query = '', $secure = array())
	{
		if( ! is_string($query) || empty($query) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'query'));
			Error::set(lang('Error', 'emptyParameter', 'query'));
			
			return false;	
		}
		
		if( isset($this->secure) )
		{
			$secure = $this->secure;	
		}
		
		return $this->db->exec($this->_querySecurity($query), $secure);
	}
	
	/******************************************************************************************
	* TRANS START                                                                             *
	*******************************************************************************************
	| Genel Kullanım: Çoklu sorgu oluşturmak için sorgu başlangıç yöntemidir.     			  |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: $this->db->transStart();        			                              |
	|          																				  |
	******************************************************************************************/
	public function transStart()
	{
		$this->transStart = $this->db->transStart();
	}
	
	/******************************************************************************************
	* TRANS END                                                                               *
	*******************************************************************************************
	| Genel Kullanım: Çoklu sorgu oluşturmak için sorgu bitiş yöntemidir.     			      |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: $this->db->transEnd();        			                              |
	|          																				  |
	******************************************************************************************/
	public function transEnd()
	{
		if( ! empty($this->transError) )
		{
			$this->db->transRollback();
		}
		else
		{
			$this->db->transCommit();
		}
		
		$this->transStart = NULL;	
		$this->transError = NULL;
	}
	
	/******************************************************************************************
	* TOTAL ROWS                                                                              *
	*******************************************************************************************
	| Genel Kullanım: Tablodaki toplam kayıt sayısını verir.     			   		          |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->totalRows();        			                                      |
	|          																				  |
	******************************************************************************************/
	public function totalRows($total = false)
	{ 
		if( $total === false )
		{
			return $this->db->numRows(); 
		}
		else
		{
			return $this->unlimitedTotalRows;	
		}
	}
	
	/******************************************************************************************
	* TOTAL COLUMNS                                                                           *
	*******************************************************************************************
	| Genel Kullanım: Tablodaki toplam sütun sayısını verir.     			   		          |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->totalColumns();      			                              		  |
	|          																				  |
	******************************************************************************************/
	public function totalColumns()
	{
		return $this->db->numFields(); 
	}
	
	/******************************************************************************************
	* COLUMNS                                                                                 *
	*******************************************************************************************
	| Genel Kullanım: Tablodaki sütun bilgilerini verir.     			   		              |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->columns();      			                                          |
	|          																				  |
	******************************************************************************************/
	public function columns()
	{ 
		return $this->db->columns(); 
	}
	
	/******************************************************************************************
	* RESULT                                                                                  *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucu kayıt bilgilerini verir.     			   		          |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->result();                			                                  |
	|          																				  |
	******************************************************************************************/
	public function result( $type = 'object' )
	{ 
		if( $type === 'object' )
		{
			return $this->db->result();
		}
		else
		{
			return $this->db->resultArray();
		}
	}
	
	/******************************************************************************************
	* RESULT ARRAY                                                                            *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucu kayıt bilgilerini dizi veri türünde elde edilir.     	  |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->resultArray();                			                              |
	|          																				  |
	******************************************************************************************/
	public function resultArray()
	{ 
		return $this->db->resultArray(); 
	}
	
	/******************************************************************************************
	* FETCH ARRAY                                                                             *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucu verileri dizi türünde verir.     	  					  |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->fetchArray();                			                              |
	|          																				  |
	******************************************************************************************/
	public function fetchArray()
	{ 
		return $this->db->fetchArray(); 
	}
	
	/******************************************************************************************
	* FETCH ASSOC                                                                             *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucu verileri object veri türünde verir.     	  				  |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->fetchAssoc();                			                              |
	|          																				  |
	******************************************************************************************/
	public function fetchAssoc()
	{ 
		return $this->db->fetchAssoc(); 
	}
	
	/******************************************************************************************
	* FETCH                                                                                   *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucu verileri object veri türünde verir.     	  				  |
		
	  @var string $type: assoc, array veya row
	|          																				  |
	******************************************************************************************/
	public function fetch($type = 'assoc')
	{ 
		if( $type === 'assoc' )
		{
			return $this->db->fetchAssoc(); 
		}
		elseif( $type === 'array')
		{
			return $this->db->fetchArray(); 
		}
		else
		{
			return $this->db->fetchRow();
		}
	}
	
	/******************************************************************************************
	* FETCH ROW                                                                               *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucu tek satır veriyi object veri türünde verir.     	  		  |
		
	  @param bool $printable: false
	  @return object/string
	|          																				  |
	******************************************************************************************/
	public function fetchRow($printable = false)
	{ 
		$row = $this->db->fetchRow();
		
		if( $printable === false )
		{
			return $row ; 
		}
		else
		{
			return current($row);	
		}
	}
	
	/******************************************************************************************
	* ROW                                                                                     *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucu tek satır veriyi elde etmek için kullanılır.     	  	  |

	  @param bool $printable: false
	  @return object/string
	|          																				  |
	******************************************************************************************/
	public function row($printable = false)
	{ 
		if( is_numeric($printable) )
		{
			$result = $this->db->resultArray(); 
			
			if( $printable < 0 )
			{
				return $result[count($result) + $printable];	
			}
			else
			{
				return $result[$printable];
			}
		}
		elseif( $printable === false )
		{
			return $this->db->row();
		}
		else
		{
			return current($this->db->row());	
		}
	}
	
	/******************************************************************************************
	* AFFECTED ROWS                                                                           *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinden etkilenen satır sayısını verir.		     	  	  |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->affectedRows();                			                          |
	|          																				  |
	******************************************************************************************/
	public function affectedRows()
	{ 
		return $this->db->affectedRows();
	}
	
	/******************************************************************************************
	* INSERT ID                                                                               *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde INSERT ID kullanımı içindir.		     	  	      |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->insertId();                			                              |
	|          																				  |
	******************************************************************************************/
	public function insertId()
	{ 
		return $this->db->insertId(); 
	}
	
	/******************************************************************************************
	* COLUMN DATA                                                                             *
	*******************************************************************************************
	| Genel Kullanım: Sorgu sonucunda tabloya ait sütun bilgilerini almak için kullanılır.	  |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->columnData();                			                              |
	|          																				  |
	******************************************************************************************/
	public function columnData()
	{ 
		return $this->db->columnData(); 
	}
	
	/******************************************************************************************
	* TABLE NAME                                                                              *
	*******************************************************************************************
	| Genel Kullanım: Sorguda kullanılan tablonun bilgisini verir.				     	  	  |
	|															                              |
	| Parametreler: Herhangi bir parametresi yoktur.                                          |
	|          																				  |
	| Örnek Kullanım: ->tableName();                			                              |
	|          																				  |
	******************************************************************************************/
	public function tableName()
	{ 
		return $this->tableName; 
	}
	
	/******************************************************************************************
	* PAGINATION                                                                              *
	*******************************************************************************************
	| Genel Kullanım: Veritabanı sorgularına göre sayfalama verilerini oluşturur.	          |
	  
	  @param  array $settings
	  @param  bool $output
	  @return array veya object
	|          																				  |
	******************************************************************************************/
	public function pagination($settings = array(), $output = true)
	{ 
		if( ! is_array($settings) )
		{
			Error::set(lang('Error', 'arrayParameter', '1.(settings)'));	
		} 
	
		$limit = $this->pagination['limit'];
		$start = $this->pagination['start'];
		
		$settings['totalRows'] = $this->unlimitedTotalRows;
		$settings['limit']     = isset($limit) ? $limit : 10;
		$settings['start']     = isset($start) ? $start : NULL;
		
		$return = $output === true
		        ? Pagination::create(NULL, $settings) 
				: $settings;
		
		$this->pagination = array('start' => 0, 'limit' => 0);
		
		return $return;
	}
	
	/******************************************************************************************
	* MATH                                                                                    *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde matematiksel yöntemlerin kullanılması içindir.		  |
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. array/string var @expression => Matematiksel yöntemlerin yazımı için kullanılır.     |
	|          																				  |
	| Örnek Kullanım: ->math( array('avg' => array('sayi', 'id')) );  // Dizi olarak          |
	| Örnek Kullanım: ->math('AVG(sayi, id)') // Metin olarak                                 |
	|          																				  |
	******************************************************************************************/
	public function math($expression = array())
	{ 
		if( ! is_array($expression) ) 
		{
			if( is_string($expression) )
			{
				$this->math = $expression;
			}
			
			return $this;
		}
		
		$exp  = ''; 
		$vals = '';
		
		if( ! empty($expression) ) foreach( $expression as $mf => $val )
		{
			$exp .= $mf.'(';
			
			if( ! empty($val) && is_array($val) ) foreach( $val as $v )
			{
				$vals .= $v.',';
			}
			
			$vals = substr($vals, 0, -1);
			$exp .= $vals.'),';
		}
		
		$math = substr($exp, 0, -1);
		
		$this->math = $math; 
		
		return $this; 
	}
	
	/******************************************************************************************
	* GROUP BY                                                                                *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde GROUP BY kullanımıdır.			                	  |
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. string var @condition => Kümelemeyi oluşturacak veri parametresi.                    |
	|          																				  |
	| Örnek Kullanım: ->groupBy('id')  // GROUP BY id								          |
	|          																				  |
	******************************************************************************************/
	public function groupBy($condition = '')
	{ 
		if( ! is_string($condition) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'condition')); 
		}
		else
		{
			$this->groupBy = ' GROUP BY '.$condition.' ' ;
		}
		
		return $this;
	}
	
	/******************************************************************************************
	* ORDER BY                                                                                *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde ORDER BY kullanımıdır.			                	  |
	|															                              |
	| Parametreler: 2 parametresi vardır.                                                     |
	| 1. string var @condition => Kümelemeyi oluşturacak veri parametresi.                    |
	| 1. string var @type => Sıralama türü.                    								  |
	|          																				  |
	| Örnek Kullanım: ->orderBy('id', 'desc')  // ORDER BY id DESC							  |
	|          																				  |
	******************************************************************************************/
	public function orderBy($condition = '', $type = '')
	{ 
		if( ! is_string($condition) || ! is_string($type) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'condition, type')); 
		}
		else
		{
			$this->orderBy = ' ORDER BY '.$condition.' '.$type.' ';  
		}
		
		return $this; 
	}
	
	/******************************************************************************************
	* LIMIT                                                                                   *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde LIMIT kullanımıdır.			                	  |
	|															                              |
	| Parametreler: 2 parametresi vardır.                                                     |
	| 1. string var @start => Limitlemeye kaçıncı kayıttan başlanacak.                        |
	| 1. string var @limit => Kaç kayıt limitlenecek.                    					  |
	|          																				  |
	| Örnek Kullanım: ->limit(0, 5)  // LIMIT 0, 5											  |
	|          																				  |
	******************************************************************************************/
	public function limit($start = 0, $limit = 0)
	{ 
		if( $start === NULL )
		{
			$start = URI::segment(-1);
		}
		
		if( ! empty($limit) ) 
		{
			$comma = ' , '; 
		}
		else 
		{
			$comma = '';
		}
		
		$this->pagination['start'] = (int)$start;
		$this->pagination['limit'] = (int)$limit;
		$this->limit = ' LIMIT '.(int)$start.$comma.(int)$limit.' ';
	
		return $this; 
	}
	
	/******************************************************************************************
	* STATUS                                                                                  *
	*******************************************************************************************
	| Genel Kullanım: Tablo hakkında bilgi almak için kullanılır.					  		  |
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. string var @table => Verilerin alınacağı tablo ismi.                                 |
	|          																				  |
	| Örnek Kullanım: $this->db->status('OrnekTablo');  									  |
	|          																				  |
	******************************************************************************************/
	public function status($table = '')
	{
		if( ! empty($this->table) ) 
		{
			$table = $this->table; 
			$this->table = NULL;
		}

		if( ! is_string($table) || empty($table) ) 
		{
			Error::set(lang('Error', 'stringParameter', 'table'));
			Error::set(lang('Error', 'emptyParameter', 'table'));
		}
		else
		{
			$table = "'".$this->prefix.trim($table)."'";
	
			$query = "SHOW TABLE STATUS FROM ".$this->config['database']." LIKE $table";
			
			$secure = $this->secure;
			
			$this->db->query($this->_querySecurity($query), $secure);
		}

		return $this;
	}
	
	/******************************************************************************************
	* PROTECTED INCREMENT VE DECREMENT                                                        *
	******************************************************************************************/
	protected function _incdec($table = '', $columns = array(), $incdec = 0, $type = '')
	{
		if( ! empty($this->table) ) 
		{
			// Table yöntemi tanımlanmış ise
			// 1. parametre, 2. parametre olarak kullanılsın
			$columns = $type === 'increment'	
					 ? abs($columns)
					 : -abs($columns);
			
			$incdec  = $columns;
			$columns = $table;
			$table   = $this->table; 
			$this->table = NULL;
		}
		
		if( ! is_string($table) || empty($columns) || ! is_numeric($incdec) )
		{
			Error::set(lang('Error', 'stringParameter', 'table'));
			Error::set(lang('Error', 'emptyParameter', 'columns'));
			Error::set(lang('Error', 'numericParameter', 'incdec'));
			
			return false;
		}
		
		$incdec = $type === 'increment'	
				 ? abs($incdec)
				 : -abs($incdec);
		
		if( is_array($columns) ) foreach( $columns as $v )
		{
			$newColumns[$v] = "$v + $incdec";	
		}
		else
		{
			$newColumns = array($columns => "$columns + $incdec");	
		}

		if( ! empty($this->where) ) 
		{
			$where = ' WHERE '; 
		}
		else 
		{
			$where = '';
		}
		
		$data = '';
		
		foreach( $newColumns as $key => $value )
		{
			$data .= $key.'='.$value.',';
		}
		
		$set = ' SET '.substr($data,0,-1);
		
		$updateQuery = 'UPDATE '.$this->prefix.$table.$set.$where.$this->where;
		
		$this->where = NULL;

		return $this->db->query($updateQuery);
	}
	
	/******************************************************************************************
	* INCREMENT                                                                               *
	*******************************************************************************************
	| Genel Kullanım: Belirtilen sütunların değerini 1 artırır.	  							  |
	|															                              |
	| Parametreler: 2 dizi parametresi vardır.                                                |
	| 1. string var @table => Tablo Adı.					 			                      |
	| 2. string/array var @columns => Bir bir artırılacak sütun veya sütunlar.                |
	| 3. numeric var @increment => Artış miktarı.               							  |
	|          																				  |
	| Örnek Kullanım: ->increment('OrnekTablo', 'Hit')				  				          |
	|          																				  |
	******************************************************************************************/
	public function increment($table = '', $columns = array(), $increment = 1)
	{
		return $this->_incdec($table, $columns, $increment, 'increment');
	}
	
	/******************************************************************************************
	* DECREMENT                                                                               *
	*******************************************************************************************
	| Genel Kullanım: Belirtilen sütunların değerini istenilen miktarda azaltır.	  		  |
	|															                              |
	| Parametreler: 2 dizi parametresi vardır.                                                |
	| 1. string var @table => Tablo Adı.					 			                      |
	| 2. string/array var @columns => Bir bir azaltılacak sütun veya sütunlar.                |
	| 3. numeric var @decrement => Azalış miktarı.               							  |
	|          																				  |
	| Örnek Kullanım: ->decrement('OrnekTablo', 'Hit')				  				          |
	|          																				  |
	******************************************************************************************/
	public function decrement($table = '', $columns = array(), $decrement = 1)
	{
		return $this->_incdec($table, $columns, $decrement, 'decrement');
	}
	
	/******************************************************************************************
	* INSERT                                                                                  *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde veri eklemek için INSERT işlemini gerçekleştirir.	  |
	|															                              |
	| Parametreler: 2 parametresi vardır.                                                     |
	| 1. string var @table => Verilerin ekleneceği tablo ismi.                                |
	| 2. array var @datas => Tabloya eklenecek veri dizisi.                                   |
	|          																				  |
	| Örnek Kullanım: $this->db->insert('OrnekTablo', array('id' => '1', 'name' => 'zntr'));  |
	|          																				  |
	******************************************************************************************/
	public function insert($table = '', $datas = array())
	{
		if( ! empty($this->table) ) 
		{
			// Table yöntemi tanımlanmış ise
			// 1. parametre, 2. parametre olarak kullanılsın
			$datas = $table;
			$table = $this->table; 
			$this->table = NULL;
		}
		
		if( ! is_string($table) || empty($table) ) 
		{
			return Error::set(lang('Error', 'stringParameter', 'table'));
		}
		
		if( ! is_array($datas) || empty($datas) ) 
		{
			return Error::set(lang('Error', 'arrayParameter', 'datas'));
		}
		
		$data = ""; $values = "";
		
		foreach($datas as $key => $value)
		{
			$data .= $key.",";
			
			if( $value !== '?' )
			{
				$values .= "'".$value."'".",";
			}
			else
			{
				$values .= $value.",";
			}
		}
			
		$insertQuery = 'INSERT INTO '.$this->prefix.$table.' ('.substr($data,0,-1).') VALUES ('.substr($values,0,-1).')';
		
		$secure = $this->secure;

		return $this->db->query($this->_querySecurity($insertQuery), $secure);
	}
	
	/******************************************************************************************
	* UPDATE                                                                                  *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde veri güncellemek için UPDATE işlemini gerçekleştirir.|
	|															                              |
	| Parametreler: 2 parametresi vardır.                                                     |
	| 1. string var @table => Verilerin güncelleneceği tablo ismi.                            |
	| 2. array var @datas => Güncellenecek veri dizisi.                                       |
	|          																				  |
	| Örnek Kullanım: $this->db->update('OrnekTablo', array('id' => '1', 'name' => 'zntr'));  |
	|          																				  |
	******************************************************************************************/
	public function update($table = '', $set = array())
	{
		if( ! empty($this->table) ) 
		{
			// Table yöntemi tanımlanmış ise
			// 1. parametre, 2. parametre olarak kullanılsın
			$set   = $table;
			$table = $this->table; 
			$this->table = NULL;
		}
		
		if( ! is_string($table) || empty($table) ) 
		{
			return Error::set(lang('Error', 'stringParameter', 'table'));
		}
		
		if( ! is_array($set) || empty($set) ) 
		{
			return Error::set(lang('Error', 'arrayParameter', 'set'));
		}
		
		if( ! empty($this->where) ) 
		{
			$where = ' WHERE '; 
		}
		else 
		{
			$where = '';
		}
		
		$data = '';
		
		foreach( $set as $key => $value )
		{
			$data .= $key.'='."'".$value."'".',';
		}
		
		$set = ' SET '.substr($data,0,-1);
		
		$updateQuery = 'UPDATE '.$this->prefix.$table.$set.$where.$this->where;
	
		$this->where = NULL;
		$secure = $this->secure;

		return $this->db->query($this->_querySecurity($updateQuery), $secure);	
	}
	
	/******************************************************************************************
	* DELETE                                                                                  *
	*******************************************************************************************
	| Genel Kullanım: Sorgu işlemlerinde veri güncellemek için DELETE işlemini gerçekleştirir.|
	|															                              |
	| Parametreler: Tek parametresi vardır.                                                   |
	| 1. string var @table => Verilerin silineceği tablo ismi.       	                      |
	|          																				  |
	| Örnek Kullanım: $this->db->delete('OrnekTablo');  									  |
	|          																				  |
	******************************************************************************************/
	public function delete($table = '')
	{
		if( ! empty($this->table) ) 
		{
			$table = $this->table; 
			$this->table = NULL;
		}
		
		if( ! is_string($table) || empty($table) ) 
		{
			return Error::set(lang('Error', 'stringParameter', 'table'));
		}
		
		if( ! empty($this->where) ) 
		{
			$where = ' WHERE '; 
		}
		else 
		{
			$where = '';
		}
		
		$deleteQuery = 'DELETE FROM '.$this->prefix.$table.$where.$this->where;
		
		$this->where = NULL;
			
		$secure = $this->secure;

		return $this->db->query($this->_querySecurity($deleteQuery), $secure);
	}
	
	/******************************************************************************************
	* DEĞİŞKENLERİ SIFIRLA                                                                    *
	******************************************************************************************/
	private function _resetQuery()
	{
		$this->all 			= NULL;
		$this->distinct 	= NULL;
		$this->distinctRow 	= NULL;
		$this->highPriority = NULL;
		$this->straightJoin = NULL;
		$this->smallResult 	= NULL;
		$this->bigResult 	= NULL;
		$this->bufferResult = NULL;
		$this->cache 		= NULL;
		$this->noCache 		= NULL;
		$this->calcFoundRows= NULL;
		$this->select 		= NULL;
		$this->selectColumn = NULL;
		$this->math 		= NULL;
		$this->from 		= NULL;
		$this->table 		= NULL;
		$this->where 		= NULL;
		$this->groupBy 		= NULL;
		$this->having 		= NULL;
		$this->orderBy 		= NULL;
		$this->limit 		= NULL;
		$this->join 		= NULL;
	}
}