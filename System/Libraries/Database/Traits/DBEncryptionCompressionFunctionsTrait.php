<?php
trait DBEncryptionCompressionFunctionsTrait
{
	/******************************************************************************************
	* AES_DECRYPT                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function aesDecrypt()
	{
		return $this->_math('AES_DECRYPT', func_get_args());
	}
	
	/******************************************************************************************
	* AES_ENCRYPT                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function aesEncrypt()
	{
		return $this->_math('AES_ENCRYPT', func_get_args());
	}
	
	/******************************************************************************************
	* COMPRESS                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function compress()
	{
		return $this->_math('COMPRESS', func_get_args());
	}
	
	/******************************************************************************************
	* DECODE                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function decode()
	{
		return $this->_math('DECODE', func_get_args());
	}
	
	/******************************************************************************************
	* ENCODE                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function encode()
	{
		return $this->_math('ENCODE', func_get_args());
	}
	
	/******************************************************************************************
	* DES_DECRYPT                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function desDecrypt()
	{
		return $this->_math('DES_DECRYPT', func_get_args());
	}
	
	/******************************************************************************************
	* DES_ENCRYPT                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function desEncrypt()
	{
		return $this->_math('DES_ENCRYPT', func_get_args());
	}
	
	/******************************************************************************************
	* ENCRYPT                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function encrypt()
	{
		return $this->_math('ENCRYPT', func_get_args());
	}
	
	/******************************************************************************************
	* MD5                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function md5()
	{
		return $this->_math('MD5', func_get_args());
	}
	
	/******************************************************************************************
	* OLD_PASSWORD                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function oldPassword()
	{
		return $this->_math('OLD_PASSWORD', func_get_args());
	}
	
	/******************************************************************************************
	* PASSWORD                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function password()
	{
		return $this->_math('PASSWORD', func_get_args());
	}
	
	/******************************************************************************************
	* RANDOM_BYTES                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function randomBytes()
	{
		return $this->_math('RANDOM_BYTES', func_get_args());
	}
	
	/******************************************************************************************
	* SHA                                                                                  *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function sha()
	{
		return $this->_math('SHA', func_get_args());
	}
	
	/******************************************************************************************
	* SHA1                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function sha1()
	{
		return $this->_math('SHA1', func_get_args());
	}
	
	/******************************************************************************************
	* SHA2                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function sha2()
	{
		return $this->_math('SHA2', func_get_args());
	}
	
	/******************************************************************************************
	* UNCOMPRESS                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function uncompress()
	{
		return $this->_math('UNCOMPRESS', func_get_args());
	}
	
	/******************************************************************************************
	* UNCOMPRESSED_LENGTH                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function uncompressLength()
	{
		return $this->_math('UNCOMPRESSED_LENGTH', func_get_args());
	}
	
	/******************************************************************************************
	* VALIDATE_PASSWORD_STRENGTH                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function validatePasswordStrength()
	{
		return $this->_math('VALIDATE_PASSWORD_STRENGTH', func_get_args());
	}
}