<?php
trait DBEnterpriseEncryptionFunctionsTrait
{
	/******************************************************************************************
	* ASYMMETRIC_DECRYPT                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function asymmetricDecrypt()
	{
		return $this->_math('ASYMMETRIC_DECRYPT', func_get_args());
	}
	
	/******************************************************************************************
	* ASYMMETRIC_DERIVE                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function asymmetricDerive()
	{
		return $this->_math('ASYMMETRIC_DERIVE', func_get_args());
	}
	
	/******************************************************************************************
	* ASYMMETRIC_ENCRYPT                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function asymmetricEncrypt()
	{
		return $this->_math('ASYMMETRIC_ENCRYPT', func_get_args());
	}
	
	/******************************************************************************************
	* ASYMMETRIC_SIGN                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function asymmetricSign()
	{
		return $this->_math('ASYMMETRIC_SIGN', func_get_args());
	}
	
	/******************************************************************************************
	* ASYMMETRIC_VERIFY                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function asymmetricVerify()
	{
		return $this->_math('ASYMMETRIC_VERIFY', func_get_args());
	}
	
	/******************************************************************************************
	* CREATE_ASYMMETRIC_PRIV_KEY                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function createAsymmetricPrivKey()
	{
		return $this->_math('CREATE_ASYMMETRIC_PRIV_KEY', func_get_args());
	}
	
	/******************************************************************************************
	* CREATE_ASYMMETRIC_PUB_KEY                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function createAsymmetricPubKey()
	{
		return $this->_math('CREATE_ASYMMETRIC_PUB_KEY', func_get_args());
	}
	
	/******************************************************************************************
	* CREATE_DH_PARAMETERS                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function createDhParameters()
	{
		return $this->_math('CREATE_DH_PARAMETERS', func_get_args());
	}
	
	/******************************************************************************************
	* CREATE_DIGEST                                                                                 *
	*******************************************************************************************
	| 																 	 					  |
	  @param args
		
	  @return string
	|          																				  |
	******************************************************************************************/
	public function createDigest()
	{
		return $this->_math('CREATE_DIGEST', func_get_args());
	}
}